package com.qantasloyalty.lsl.file.transfer.crypt

import arrow.core.identity
import ch.tutteli.atrium.api.cc.en_GB.returnValueOf
import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.assertThat
import com.qantasloyalty.lsl.file.transfer.junit.configurations.FileTransferIntegrationTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.springframework.beans.factory.annotation.Autowired
import java.nio.file.Path
import java.nio.file.Paths

@FileTransferIntegrationTest
class DecryptServiceIT {

    @Autowired
    private lateinit var decryptService: DecryptService

    @Test
    fun `should decrypt an encrypted file successfully`() {
        val file = Paths.get(
            DecryptServiceIT::class.java.classLoader
                .getResource("incoming/QASRNIB_RetentionCampaign_Reactive_Lapse_20171006.pgp")
            !!.toURI()
        )

        val testResource = decryptService.decryptFile(file, WORKING_DIRECTORY)
            .attempt()
            .unsafeRunSync()
            .fold({ throw it }, ::identity)

        assertThat(testResource) {
            returnValueOf(subject::getFileName) {
                returnValueOf(subject::toString).toBe("QASRNIB_RetentionCampaign_Reactive_Lapse_20171006.pgp.plain")
            }
        }
    }

    companion object {

        @JvmStatic
        @TempDir
        lateinit var WORKING_DIRECTORY: Path
    }
}