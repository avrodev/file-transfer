package com.qantasloyalty.lsl.file.transfer.junit.configurations

import com.qantasloyalty.lsl.file.transfer.FileTransferApplication
import io.github.glytching.junit.extension.random.RandomBeansExtension
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE

@SpringBootTest(
    classes = [FileTransferApplication::class],
    webEnvironment = NONE
)
@ExtendWith(value = [RandomBeansExtension::class])
annotation class FileTransferIntegrationTest