package com.qantasloyalty.lsl.file.transfer

import com.qantasloyalty.lsl.logging.FluentLogger
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import java.security.Security.addProvider
import java.security.Security.getProvider

@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan(
    value = [
        "com.qantasloyalty.lsl.file.transfer",
        "com.qantasloyalty.lsl.logging"
    ]
)
class FileTransferApplication {
    companion object {

        private val LOGGER = FluentLogger.forClass(FileTransferApplication::class.java)

        init {
            if (getProvider(PROVIDER_NAME) == null) {
                addProvider(BouncyCastleProvider())
                LOGGER.info().message("Registered Bouncy Castle Provider")
            }
        }

        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<FileTransferApplication>(*args)
        }
    }
}