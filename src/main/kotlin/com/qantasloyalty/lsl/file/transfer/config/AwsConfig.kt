package com.qantasloyalty.lsl.file.transfer.config

import com.amazonaws.services.secretsmanager.AWSSecretsManager
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class AwsConfig {

    @Value("\${aws.region}")
    private lateinit var region: String

    @Bean
    internal fun secretsManager(): AWSSecretsManager =
        AWSSecretsManagerClientBuilder.standard()
//            .withRegion(region)
            .build()
}