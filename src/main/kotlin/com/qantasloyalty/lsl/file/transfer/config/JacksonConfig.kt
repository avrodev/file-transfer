package com.qantasloyalty.lsl.file.transfer.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.qantasloyalty.lsl.file.transfer.utils.ObjectMapperFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class JacksonConfig {

    @Bean
    internal fun objectMapper(): ObjectMapper = ObjectMapperFactory.createObjectMapper()
}