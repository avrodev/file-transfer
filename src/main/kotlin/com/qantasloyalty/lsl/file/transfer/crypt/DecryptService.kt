package com.qantasloyalty.lsl.file.transfer.crypt

import arrow.core.toOption
import arrow.effects.IO
import arrow.effects.IODispatchers
import arrow.effects.extensions.io.fx.fx
import com.google.common.base.Stopwatch
import com.qantasloyalty.lsl.file.transfer.aws.secrets.GpgSecrets
import com.qantasloyalty.lsl.file.transfer.aws.secrets.SecretService
import com.qantasloyalty.lsl.logging.FluentLogger
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.BouncyGPG
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.callbacks.KeyringConfigCallbacks
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.keyrings.KeyringConfigs
import org.apache.commons.lang3.StringUtils.removeEndIgnoreCase
import org.bouncycastle.util.io.Streams
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.io.BufferedOutputStream
import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.TimeUnit

@Component
class DecryptService @Autowired constructor(
    private val secretService: SecretService,
    @Value("\${lsl.decrypt.allowCleanText}") private val allowCleanText: Boolean
) {

    fun decryptFile(maybeEncrypedFile: Path, workingDirectory: Path): IO<Path> =
        fx {
            check(Files.isRegularFile(maybeEncrypedFile)) { "$maybeEncrypedFile is not a file" }
            check(Files.isDirectory(workingDirectory)) { "$workingDirectory is not a directory" }

            val stopwatch = Stopwatch.createStarted()

            LOGGER.debug().message("Start processing $maybeEncrypedFile")

            val (isTextFileFiber) = IODispatchers.CommonPool.startFiber(isTextFile(maybeEncrypedFile))

            val (destinationFileFiber) = IODispatchers.CommonPool.startFiber(
                getCleanDestinationFile(
                    maybeEncrypedFile,
                    workingDirectory
                )
            )

            val target = if (!isTextFileFiber.join()) {
                LOGGER.debug().message("$maybeEncrypedFile appears to be in clean text")

                !copyTextFile(maybeEncrypedFile, !destinationFileFiber.join())
            } else {
                LOGGER.debug().message("$maybeEncrypedFile appears to NOT be in clean text, attempt decryption")

                val (gpgSecretsFiber) = IODispatchers.CommonPool.startFiber(secretService.getGpgSecrets())

                !decrypt(maybeEncrypedFile, !destinationFileFiber.join(), !gpgSecretsFiber.join())
            }

            stopwatch.stop()

            LOGGER.debug()
                .message("Processing of $maybeEncrypedFile to $target took ${stopwatch.elapsed(TimeUnit.MILLISECONDS)}ms")

            target
        }

    private fun getCleanDestinationFile(sourceFile: Path, workingDirectory: Path): IO<Path> = IO {
        val basename = removeEndIgnoreCase(
            requireNotNull(sourceFile.fileName, { "$sourceFile has no filename" }).toString().trim(),
            ".DAT.PGP"
        )
        val destinationFile = workingDirectory.resolve("$basename.plain")

        LOGGER.debug().message("$sourceFile will be decrypted to $destinationFile")

        if (Files.deleteIfExists(destinationFile)) {
            LOGGER.warn().message("$destinationFile already existed, and been deleted...")
        }

        destinationFile
    }

    private fun isTextFile(sourceFile: Path): IO<Boolean> = IO {
        Files.probeContentType(sourceFile).toOption()
            .exists { it.contains("text", ignoreCase = true) }
    }

    private fun copyTextFile(source: Path, target: Path): IO<Path> = IO {
        check(allowCleanText) { "$source is clean text, but this is not allowed" }
        Files.copy(source, target)
    }

    private fun decrypt(source: Path, target: Path, gpgSecrets: GpgSecrets): IO<Path> = IO {
        val keyringConfig =
            KeyringConfigs.forGpgExportedKeys(KeyringConfigCallbacks.withPassword(gpgSecrets.passphrase))

        keyringConfig.addSecretKey(gpgSecrets.secretKey)

        keyringConfig.addPublicKey(gpgSecrets.publicKey)

        Files.newInputStream(source).use { cipherTextStream ->
            BufferedOutputStream(Files.newOutputStream(target)).use { fileOutput ->
                BouncyGPG.decryptAndVerifyStream()
                    .withConfig(keyringConfig)
                    .andIgnoreSignatures()
                    .fromEncryptedInputStream(cipherTextStream).use { plaintextStream ->
                        Streams.pipeAll(plaintextStream, fileOutput)
                    }
            }
        }

        target
    }

    companion object {

        private val LOGGER = FluentLogger.forClass(DecryptService::class.java)
    }
}