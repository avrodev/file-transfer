package com.qantasloyalty.lsl.file.transfer.utils

import com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL
import com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
import com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule

object ObjectMapperFactory {

    fun createObjectMapper() =
        jacksonObjectMapper()
            .apply {
                disable(FAIL_ON_UNKNOWN_PROPERTIES)
                disable(WRITE_DATES_AS_TIMESTAMPS)
                setSerializationInclusion(NON_NULL)
                registerModules(JavaTimeModule(), ParameterNamesModule())
            }
}