package com.qantasloyalty.lsl.file.transfer.aws.secrets

import arrow.core.Option
import arrow.core.Try
import arrow.core.failure
import arrow.core.handleErrorWith
import arrow.core.toOption
import arrow.effects.IO
import arrow.effects.IODispatchers
import arrow.effects.extensions.io.fx.fx
import com.amazonaws.services.secretsmanager.AWSSecretsManager
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.qantasloyalty.lsl.file.transfer.crypt.DecryptService
import com.qantasloyalty.lsl.logging.FluentLogger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.IOException
import java.util.Base64
import java.util.concurrent.atomic.AtomicReference

@Component
class SecretService @Autowired constructor(
    private val awsSecretManager: AWSSecretsManager,
    private val objectMapper: ObjectMapper
) {

    fun getGpgSecrets(): IO<GpgSecrets> =
        fx {
            val prev = SECRET_REFERENCE.get()
            if (prev != null) {
                prev
            } else {
                val (secretValueResultFiber) = IODispatchers.CommonPool.startFiber(getSecretResult())

                val (publicResultFiber) = IODispatchers.CommonPool.startFiber(getPublicResult())

                val secretValueResult = !secretValueResultFiber.join()

                secretValueResult.secretString.toOption()
                    .map { objectMapper.readTree(it) }
                    .flatMap { it.toGpgSecrets(!publicResultFiber.join()) }
                    .getOrRaiseError { IOException("Could not retrieve secrets from SecretsManager. Key: $SECRET_KEY") }
                    .bind()
                    .also { SECRET_REFERENCE.set(it) }
            }
        }

    private fun getSecretResult(): IO<GetSecretValueResult> = IO {
        val getSecretValueRequest = GetSecretValueRequest()
            .withSecretId(SECRET_KEY)

        awsSecretManager.getSecretValue(getSecretValueRequest)
    }

    private fun getPublicResult(): IO<ByteArray> = IO {
        DecryptService::class.java.classLoader.getResourceAsStream("publicKeys/AssurePublicKey.pub")
            ?.readBytes()
            ?: throw IOException("Public Key resource not found")
    }

    private fun JsonNode.toGpgSecrets(publicKey: ByteArray): Option<GpgSecrets> =
        Try {
            GpgSecrets(
                publicKey = publicKey,
                secretKey = Base64.getDecoder().decode(this["secretKey"].asText()),
                passphrase = this["passphrase"].asText()
            )
        }.handleErrorWith {
            LOGGER.error().cause(it).message("Failed to handle GpgSecrets result")
            it.failure()
        }.toOption()

    companion object {
        private val LOGGER = FluentLogger.forClass(SecretService::class.java)

        private const val SECRET_KEY = "dev/file-transfer/secrets"
        private val SECRET_REFERENCE = AtomicReference<GpgSecrets>()
    }
}