package com.qantasloyalty.lsl.file.transfer.aws.secrets

data class GpgSecrets(
    val secretKey: ByteArray,
    val publicKey: ByteArray,
    val passphrase: String
)